# ACR122UAPI

Português
=========

### Introdução

ACR122UAPI é uma biblioteca Java escrita sobre a API Java Smart Card I/O
cujo objetivo é o de facilitar a comunicação com o dispositivo ACS ACR122U
([mais informações]
(http://www.acs.com.hk/en/products/3/acr122u-usb-nfc-reader/)). A biblioteca
disponibiliza uma classe (ACR122UDevice), usada para a comunicação com o
hardware que envia eventos como a detecção de cartão.

English
=======

### Introduction

ACR122UAPI is a Java library written over Java Smart Card I/O API for
communication with ACS ACR122U device ([more information]
(http://www.acs.com.hk/en/products/3/acr122u-usb-nfc-reader/)). The library
provides a class (ACR122UDevice) used for communication with hardware, which
send events like card detection.

## Instruções / Instructions

    git clone https://gitlab.com/josevolpato/ACR122UAPI.git
    cd ACR122UAPI
    ./gradle sourcesJar
    

## Tarefas do gradle / Useful gradle tasks

* _jar_ - Gera o arquivo .jar  / Generate .jar file
* _sourceJar_ - Gera o arquivo .jar com os códigos-fonte / Generate .jar file
with sources
* _clean_ - Limpa o diretório de compilação / Clean build directory

## Exemplo / Usage

    ACR122UDevice device = ACR122UDevice.getDevice();
    device.setOnDeviceConnected(new OnDeviceConnectedEventHandler() {
    
        @Override
        public void onDeviceConnected()
        {
            System.out.println("Device connected!");
        }
    
    });
    device.setOnDeviceDisconnected(new OnDeviceDisconnectedEventHandler() {
    
        @Override
        public void onDeviceDisconnected()
        {
            System.out.println("Device disconnected!");
            device.release(); // Stops event thread
        }
    
    });
    device.setOnCardEntered(new OnCardEnteredEventHandler() {
    
        @Override
        public void onCardEntered()
        {
            try
            {
                System.out.println("Card entered!");
                System.out.println("UID: " + device.getCardUID());
            }
            catch (CardException ex)
            {
                Logger.getLogger(ACRTest.class.getName()).
                        log(Level.SEVERE, null, ex);
            }
        }
    
    });
    device.setOnCardExited(new OnCardExitedEventHandler() {
    
        @Override
        public void onCardExited()
        {
            System.out.println("Card exited!");
        }
    
    });


## Termos de uso / Legal

MIT License

Copyright (c) 2017 José Volpato
