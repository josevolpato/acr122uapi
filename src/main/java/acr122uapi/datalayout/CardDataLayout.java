package acr122uapi.datalayout;

import java.util.ArrayList;


/**
 * 
 * @author jhcv
 */
public class CardDataLayout
{
    private final int NUMBEROFSECTORS = 16;
    private ArrayList<CardSectorDataLayout> sectors;
    
    
    public CardDataLayout()
    {
        this.sectors = new ArrayList<>();
        for (int i = 0; i < NUMBEROFSECTORS; i++)
        {
            this.sectors.add(new CardSectorDataLayout());
        }
    }
    
    
    /**
     * 
     * 
     * @param sectorIndex
     * 
     * @return 
     */
    public CardSectorDataLayout getSector(int sectorIndex)
    {
        if (sectorIndex < 0 || sectorIndex >= NUMBEROFSECTORS)
        {
            throw new IllegalArgumentException("sectorIndex need to be between 0 and 15 inclusive.");
        }
        
        return this.sectors.get(sectorIndex);
    }
    
    /**
     * 
     * 
     * @param sector
     * @param sectorIndex
     * 
     */
    public void setSector(CardSectorDataLayout sector, int sectorIndex)
    {
        if (sectorIndex < 0 || sectorIndex >= NUMBEROFSECTORS)
        {
            throw new IllegalArgumentException("sectorIndex need to be between 0 and 15 inclusive.");
        }
        
        this.sectors.set(sectorIndex, sector);
    }
    
}
