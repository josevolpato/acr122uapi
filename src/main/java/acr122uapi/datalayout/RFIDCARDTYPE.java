/**
 * RFIDCARDTYPE.java
 * 
 * Copyright (c) 2017, José Volpato. MIT License.
 */
package acr122uapi.datalayout;


/**
 * Defines RFID card types.
 * 
 * @author José Volpato
 */
public enum RFIDCARDTYPE
{
    MIFARE1K
}
