package acr122uapi.datalayout;


/**
 * Defines a data layout of a block, for use in read/write operations.
 * 
 * @author jhcv
 */
public class CardBlockDataLayout
{
    private final int BLOCKSIZEINBYTES = 16;
    private byte[] block;
    
    
    /**
     * Create a default block data layout.
     */
    public CardBlockDataLayout()
    {
        this.block = new byte[BLOCKSIZEINBYTES];
        for (int i = 0; i < BLOCKSIZEINBYTES; i++)
        {
            this.block[i] = (byte) 0x00;
        }
    }
    
    
    /**
     * Returns a byte array representing a data block.
     * 
     * @return Data block.
     */
    public byte[] getBlock()
    {
        return this.block;
    }
    
    /**
     * Set a value to a data block.
     * 
     * @param block New data block value.
     */
    public void setBlock(byte[] block)
    {
        if (block.length != BLOCKSIZEINBYTES)
        {
            throw new IllegalArgumentException("Block length need to be " + BLOCKSIZEINBYTES);
        }
        
        for (int i = 0; i < BLOCKSIZEINBYTES; i++)
        {
            if (block[i] < (byte) 0x00 || block[i] > (byte) 0xFF)
            {
                throw new IllegalArgumentException("Byte value need to between 0x00 and 0xFF inclusive");
            }
        }
        
        this.block = block;
    }
    
    /**
     * Returns a String representing a data block.
     * 
     * @return Data block.
     */
    public String getBlockToHexString()
    {
        StringBuilder sbKey = new StringBuilder();
        for (int i = 0; i < BLOCKSIZEINBYTES; i++)
        {
            sbKey.append(String.format("%02X", this.block[i]));
        }
        return sbKey.toString();
    }
    
    /**
     * Parses a String into a data block.
     * 
     * @param block A String contining hex values that form a data block.
     */
    public void setBlockFromHexString(String block)
    {
        if (!block.matches("/^[0-9a-fA-F]{16}$/"))
        {
            throw new IllegalArgumentException("block need to be length 16, valid hex value.");
        }
        
        for (int i = 0; i < BLOCKSIZEINBYTES; i++)
        {
            this.block[i] = Byte.parseByte(block.substring((i * 2), ((i * 2) + 1)), 16);
        }
    }
    
}
