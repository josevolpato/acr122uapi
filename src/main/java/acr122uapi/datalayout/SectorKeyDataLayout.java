package acr122uapi.datalayout;


/**
 * Defines a data layout for a sector key.
 * 
 * @author jhcv
 */
public class SectorKeyDataLayout
{
    private final int KEYSIZESINBYTES = 6;
    private byte[] key;
    
    
    /**
     * Create a default sector key data layout.
     */
    public SectorKeyDataLayout()
    {
        this.key = new byte[KEYSIZESINBYTES];
        for (int i = 0; i < KEYSIZESINBYTES; i++)
        {
            this.key[i] = (byte) 0x00;
        }
    }
    
    
    /**
     * Return a byte array representing the value of this key.
     * 
     * @return This key.
     */
    public byte[] getKey()
    {
        return this.key;
    }
    
    /**
     * Set a value to this key.
     * 
     * @param key New value. Keys have length == 6.
     */
    public void setKey(byte[] key)
    {
        if (key.length != KEYSIZESINBYTES)
        {
            throw new IllegalArgumentException("Key length need to be " + KEYSIZESINBYTES);
        }
        
        for (int i = 0; i < KEYSIZESINBYTES; i++)
        {
            if (key[i] < (byte) 0x00 || key[i] > (byte) 0xFF)
            {
                throw new IllegalArgumentException("Byte value need to between 0x00 and 0xFF inclusive");
            }
        }
        
        this.key = key;
    }
    
    /**
     * Return this key in a form of a String hex values.
     * 
     * @return String representing this key.
     */
    public String getKeyToHexString()
    {
        StringBuilder sbKey = new StringBuilder();
        for (int i = 0; i < KEYSIZESINBYTES; i++)
        {
            sbKey.append(String.format("%02X", this.key[i]));
        }
        return sbKey.toString();
    }
    
    /**
     * Parse a String into a key.
     * 
     * @param key String representing a new value for this key. Key need to be
     * length == 12, with valid hex values (0-9, a-f, A-F).
     */
    public void setKeyFromHexString(String key)
    {
        if (!key.matches("^[0-9a-fA-F]{12}$"))
        {
            throw new IllegalArgumentException("Key need to be length 12, valid hex value.");
        }
        
        for (int i = 0; i < KEYSIZESINBYTES; i++)
        {
            this.key[i] = Byte.parseByte(key.substring((i * 2), ((i * 2) + 1)), 16);
        }
    }
    
}
