/**
 * RFIDSECTORKEYTYPE.java
 * 
 * Copyright (c) 2017, José Volpato. MIT License.
 */
package acr122uapi.datalayout;


/**
 * Defines sector key types.
 * 
 * @author jhcv
 */
public enum RFIDSECTORKEYTYPE
{
    TYPE_A,
    TYPE_B
}
