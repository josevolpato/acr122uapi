package acr122uapi.datalayout;

import java.util.ArrayList;


/**
 * Defines a data layout of a sector, for use in read/write operations.
 * 
 * @author jhcv
 */
public class CardSectorDataLayout
{
    private final int BLOCKSPERSECTOR = 4;
    private SectorKeyDataLayout keyA;
    private SectorKeyDataLayout keyB;
    private ArrayList<CardBlockDataLayout> blocks;
    
    
    /**
     * Create a default sector data layout.
     */
    public CardSectorDataLayout()
    {
        this.keyA = new SectorKeyDataLayout();
        this.keyB = new SectorKeyDataLayout();
        this.blocks = new ArrayList<>();
        for (int i = 0; i < BLOCKSPERSECTOR; i++)
        {
            this.blocks.add(new CardBlockDataLayout());
        }
    }
    
    
    /**
     * Return KeyA 
     * 
     * @return 
     */
    public SectorKeyDataLayout getKeyA()
    {
        return this.keyA;
    }
    
    /**
     * 
     * 
     * @param keyA
     */
    public void setKeyA(SectorKeyDataLayout keyA)
    {
        this.keyA = keyA;
    }
    
    /**
     * 
     * 
     * @return 
     */
    public SectorKeyDataLayout getKeyB()
    {
        return this.keyB;
    }
    
    /**
     * 
     * 
     * @param keyB
     */
    public void setKeyB(SectorKeyDataLayout keyB)
    {
        this.keyB = keyB;
    }
    
    /**
     * 
     * 
     * @param blockIndex
     * 
     * @return 
     */
    public CardBlockDataLayout getBlock(int blockIndex)
    {
        if (blockIndex < 0 || blockIndex >= BLOCKSPERSECTOR)
        {
            throw new IllegalArgumentException("blockIndex need to be between 0 and 3 inclusive.");
        }
        
        return this.blocks.get(blockIndex);
    }
    
    /**
     * 
     * @param block 
     * @param blockIndex
     */
    public void setBlock(CardBlockDataLayout block, int blockIndex)
    {
        if (blockIndex < 0 || blockIndex >= BLOCKSPERSECTOR)
        {
            throw new IllegalArgumentException("blockIndex need to be between 0 and 3 inclusive.");
        }
        
        this.blocks.set(blockIndex, block);
    }
    
}
