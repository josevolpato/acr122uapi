/**
 * OnCardExitedEventHandler.java
 * 
 * Copyright (c) 2017, José Volpato. MIT License.
 */
package acr122uapi.event;


/**
 * @author José Volpato
 */
public interface OnCardExitedEventHandler
{
    /**
     * A method called when a smartcard leave the active reader detection field.
     */
    public void onCardExited();
}
