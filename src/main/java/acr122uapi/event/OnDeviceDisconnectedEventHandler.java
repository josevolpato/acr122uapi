/**
 * OnDeviceDisconnectedEventHandler.java
 * 
 * Copyright (c) 2017, José Volpato. MIT License.
 */
package acr122uapi.event;


/**
 * @author José Volpato
 */
public interface OnDeviceDisconnectedEventHandler
{
    /**
     * A method called when a ACR122U device is disconnected to the computer.
     */
    public void onDeviceDisconnected();
}
