/**
 * OnDeviceConnectedEventHandler.java
 * 
 * Copyright (c) 2017, José Volpato. MIT License.
 */
package acr122uapi.event;


/**
 * @author José Volpato
 */
public interface OnDeviceConnectedEventHandler
{
    /**
     * A method called when a ACR122U device is connected to the computer.
     */
    public void onDeviceConnected();
}
