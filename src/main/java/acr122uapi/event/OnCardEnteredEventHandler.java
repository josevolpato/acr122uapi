/**
 * OnCardEnteredEventHandler.java
 * 
 * Copyright (c) 2017, José Volpato. MIT License.
 */
package acr122uapi.event;


/**
 * @author José Volpato
 */
public interface OnCardEnteredEventHandler
{
    /**
     * A method called when a smartcard enters the active reader detection 
     * field.
     */
    public void onCardEntered();
}
