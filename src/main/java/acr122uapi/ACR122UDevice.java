/**
 * ACR122UDevice.java
 * 
 * Copyright (c) 2017, José Volpato. MIT License.
 */
package acr122uapi;

import acr122uapi.commands.PICCCommands;
import acr122uapi.datalayout.CardBlockDataLayout;
import acr122uapi.datalayout.CardDataLayout;
import acr122uapi.datalayout.CardSectorDataLayout;
import acr122uapi.datalayout.KEYLOCATION;
import acr122uapi.datalayout.SectorKeyDataLayout;
import acr122uapi.event.OnCardEnteredEventHandler;
import acr122uapi.event.OnCardExitedEventHandler;
import acr122uapi.event.OnDeviceConnectedEventHandler;
import acr122uapi.event.OnDeviceDisconnectedEventHandler;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.smartcardio.Card;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.CommandAPDU;
import javax.smartcardio.ResponseAPDU;
import javax.smartcardio.TerminalFactory;


/**
 * @author José Volpato
 */
public class ACR122UDevice
{
    private static ACR122UDevice device;
    
    private final long THREAD_DELAY = 350;
    
    private boolean readerPresent;
    private boolean cardPresent;
    private Thread cardReaderThread;
    
    private CardTerminal acr122uTerminal;
    
    private OnDeviceConnectedEventHandler onDeviceConnectedEventHandler;
    private OnDeviceDisconnectedEventHandler onDeviceDisconnectedEventHandler;
    private OnCardEnteredEventHandler onCardEnteredEventHandler;
    private OnCardExitedEventHandler onCardExitedEventHandler;
    
    
    /**
     * @author José Volpato
     */
    class DeviceEventThread implements Runnable
    {
        @Override
        public void run()
        {
            while (true)
            {
                try
                {
                    TerminalFactory terminalFactory = 
                            TerminalFactory.getDefault();
                    List<CardTerminal> terminals = terminalFactory.
                                                            terminals().list();
                    
                    if (terminals.isEmpty())
                    {
                        if (readerPresent)
                        {
                            readerPresent = false;
                            if (cardPresent)
                            {
                                cardPresent = false;
                                fireCardExitedEvent();
                            }
                            fireDeviceDisconnectedEvent();
                            Thread.sleep(THREAD_DELAY);
                            continue;
                        }
                        else
                        {
                            Thread.sleep(THREAD_DELAY);
                            continue;
                        }
                    }
                    
                    boolean findReaderInList = false;
                    acr122uTerminal = null;
                    for (CardTerminal terminal : terminals)
                    {
                        if (terminal.getName().contains("ACR122"))
                        {
                            findReaderInList = true;
                            acr122uTerminal = terminal;
                            break;
                        }
                    }
                    
                    if (!findReaderInList && readerPresent)
                    {
                        readerPresent = false;
                        if (cardPresent)
                        {
                            cardPresent = false;
                            fireCardExitedEvent();
                        }
                        fireDeviceDisconnectedEvent();
                        Thread.sleep(THREAD_DELAY);
                        continue;
                    }
                    
                    if (findReaderInList && !readerPresent)
                    {
                        readerPresent = true;
                        fireDeviceConnectedEvent();
                        Thread.sleep(THREAD_DELAY);
                        continue;
                    }
                    
                    if (!acr122uTerminal.isCardPresent())
                    {
                        if (cardPresent)
                        {
                            cardPresent = false;
                            fireCardExitedEvent();
                            Thread.sleep(500);
                            continue;
                        }
                    }
                    
                    if (acr122uTerminal.isCardPresent())
                    {
                        if (!cardPresent)
                        {
                            cardPresent = true;
                            fireCardEnteredEvent();
                            Thread.sleep(500);
                            continue;
                        }
                    }
                    
                    Thread.sleep(THREAD_DELAY);
                }
                catch (CardException ex)
                {
                    Logger.getLogger(ACR122UDevice.class.getName()).
                            log(Level.SEVERE, null, ex);
                }
                catch (InterruptedException ex)
                {
                    Logger.getLogger(DeviceEventThread.class.getName()).
                            log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    
    /**
     * Private contructor that reset all reader information, create a new event 
     * thread and start event notification.
     */
    private ACR122UDevice()
    {
        this.readerPresent = false;
        this.cardPresent = false;
        
        this.onDeviceConnectedEventHandler = null;
        this.onDeviceDisconnectedEventHandler = null;
        this.onCardEnteredEventHandler = null;
        this.onCardExitedEventHandler = null;
        
        this.cardReaderThread = new Thread(new DeviceEventThread());
        this.cardReaderThread.start();
    }
    
    
    /**
     * Provide a unique instance of this class, which is responsible to provide 
     * methods to interact with ACR122U device and smartcards.
     * 
     * @return A ACR122UDevice unique instance.
     */
    public static ACR122UDevice getDevice()
    {
        if (device == null)
        {
            device = new ACR122UDevice();
        }
        return device;
    }
    
    /**
     * Verify if a smartcard is in reader's detection field.
     * 
     * @return true if a smartcard is in reader's field, false otherwise.
     */
    public boolean isCardPresent()
    {
        return cardPresent;
    }
    
    /**
     * Define a event handler for a device connected event.
     * 
     * @param onDeviceConnectedEventHandler Handler of a device connected event.
     */
    public void setOnDeviceConnected(OnDeviceConnectedEventHandler 
            onDeviceConnectedEventHandler)
    {
        this.onDeviceConnectedEventHandler = onDeviceConnectedEventHandler;
    }
    
    /**
     * Fire a device connected event. It's happen when a ARC122U is connected on
     * an usb port.
     */
    private void fireDeviceConnectedEvent()
    {
        if (this.onDeviceConnectedEventHandler != null)
        {
            this.onDeviceConnectedEventHandler.onDeviceConnected();
        }
    }
    
    /**
     * Define a event handler for a device disconnected event.
     * 
     * @param onDeviceDisconnectedEventHandler Handler of a device disconnected
     * event.
     */
    public void setOnDeviceDisconnected(OnDeviceDisconnectedEventHandler
            onDeviceDisconnectedEventHandler)
    {
        this.onDeviceDisconnectedEventHandler = 
                onDeviceDisconnectedEventHandler;
    }
    
    /**
     * Fire a device disconnected event. It's happen when a ARC122U is removed 
     * of an usb port.
     */
    private void fireDeviceDisconnectedEvent()
    {
        if (this.onDeviceDisconnectedEventHandler != null)
        {
            this.onDeviceDisconnectedEventHandler.onDeviceDisconnected();
        }
    }
    
    /**
     * Define a event handler for a card entered event.
     * 
     * @param onCardEnteredEventHandler Handler of a card entered event.
     */
    public void setOnCardEntered(OnCardEnteredEventHandler
            onCardEnteredEventHandler)
    {
        this.onCardEnteredEventHandler = onCardEnteredEventHandler;
    }
    
    /**
     * Fire the card entered event. It's happen when a smartcard is detected by
     * an active reader.
     */
    private void fireCardEnteredEvent()
    {
        if (this.onCardEnteredEventHandler != null)
        {
            this.onCardEnteredEventHandler.onCardEntered();
        }
    }
    
    /**
     * Define a event handler for a card exited event.
     * 
     * @param onCardExitedEventHandler Handler of a card exited event.
     */
    public void setOnCardExited(OnCardExitedEventHandler
            onCardExitedEventHandler)
    {
        this.onCardExitedEventHandler = onCardExitedEventHandler;
    }
    
    /**
     * Fire the card exited event. It's happen when a smartcard leave the 
     * detection field of an active reader.
     */
    private void fireCardExitedEvent()
    {
        if (this.onCardExitedEventHandler != null)
        {
            this.onCardExitedEventHandler.onCardExited();
        }
    }
    
    /**
     * Stop the event thread, reset all device information and close 
     * comunication with hardware. Can be reopened with #getDevice method.
     */
    public void release()
    {
        if (this.cardReaderThread.isAlive())
        {
            this.cardReaderThread.interrupt();
            device = null;
            this.onDeviceConnectedEventHandler = null;
            this.onDeviceDisconnectedEventHandler = null;
            this.onCardEnteredEventHandler = null;
            this.onCardExitedEventHandler = null;
        }
    }
    
    
    /**
     * Get a UID of a smartcard.
     * 
     * @return Hex formatted string containing UID of a smartcard.
     * 
     * @throws CardException 
     */
    public String getCardUID() throws CardException
    {
        CardTerminal acr122uTerminal = null;
        TerminalFactory terminalFactory = TerminalFactory.getDefault();
        List<CardTerminal> terminals = terminalFactory.terminals().list();
        for (CardTerminal terminal : terminals)
        {
            if (terminal.getName().contains("ACR122"))
            {
                acr122uTerminal = terminal;
                break;
            }
        }
        
        if (acr122uTerminal == null)
        {
            throw new CardException("ACR122 device not found in terminal list");
        }
        
        Card card = acr122uTerminal.connect("*");
        CardChannel cardChannel = card.getBasicChannel();
        CommandAPDU capdu = new CommandAPDU(PICCCommands.UID);
        ResponseAPDU response = cardChannel.transmit(capdu);
        card.disconnect(false);
        
        StringBuilder sb = new StringBuilder(response.getData().length * 2);
        for(byte b: response.getData())
        {
            sb.append(String.format("%02x", b));
        }
        
        return sb.toString();
    }
    
    /**
     * Load an authentication key to the reader.
     * 
     * @param key Hex formatted string containing a key.
     * @param keyLocation Location where key will be stored.
     * 
     * @return true - Key loaded succefully.
     * false - An error occoured while loading key.
     */
    public boolean loadAuthenticationKey(SectorKeyDataLayout key,
                                         KEYLOCATION keyLocation)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try
        {
            if (keyLocation == KEYLOCATION.LOCATION_0)
            {
                baos.write(PICCCommands.LOAD_AUTHENTICATION_KEY_0);
            }
            else
            {
                baos.write(PICCCommands.LOAD_AUTHENTICATION_KEY_1);
            }
            baos.write(key.getKey());
        }
        catch (IOException ex)
        {
            Logger.getLogger(ACR122UDevice.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            Card card = acr122uTerminal.connect("*");
            CardChannel channel = card.getBasicChannel();
            ResponseAPDU res = channel.transmit(new CommandAPDU(baos.toByteArray()));
            int sw1 = res.getSW1();
            int sw2 = res.getSW2();
            card.disconnect(false);
            return sw1 == PICCCommands.SUCCESS[0] &&
                   sw2 == PICCCommands.SUCCESS[1];
            
        } catch (CardException ex) {
            Logger.getLogger(ACR122UDevice.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;
    }
    
    
    public boolean authenticateSector()
    {
        return true;
    }
    
    /**
     * Read an entire smart card content and store it on a hex formatted string.
     * 
     * @param keys A list of all keys used in sector authentication.
     * 
     * @return Hex formatted string containing all the content of a smart card.
     */
    public CardDataLayout readAllCard(ArrayList<SectorKeyDataLayout> keys)
    {
        // TODO implement readAllCard
        throw new UnsupportedOperationException();
    }
    
    /**
     * Read a sector from a smart card and store it on a hex formatted string.
     * 
     * @param key Sector authentication key.
     * @param sectorIndex Sector index.
     * 
     * @return Hex formatted string containing a sector data.
     */
    public CardSectorDataLayout readSector(SectorKeyDataLayout key, int sectorIndex)
    {
        // TODO implement readSector
        throw new UnsupportedOperationException();
    }
    
    /**
     * Read a block from a smart card and store it on a hex formatted string.
     * 
     * @param key Sector authentication key.
     * @param sectorIndex Sector index.
     * @param blockIndex Block index.
     * 
     * @return Hex formatted string containing a block data.
     * 
     * @throws javax.smartcardio.CardException
     */
    public CardBlockDataLayout readBlock(SectorKeyDataLayout key,
                                         int sectorIndex,
                                         int blockIndex) throws CardException
    {
        throw new UnsupportedOperationException();
    }
    
}
