/**
 * PICCCommands.java
 *
 * Copyright (c) 2017, José Volpato. MIT License.
 */
package acr122uapi.commands;


/**
 * @author José Volpato
 */
public class PICCCommands
{
    /**
     * Success response command bytes.
     */
    public static final byte[] SUCCESS = { (byte) 0x90, (byte) 0x00 };
    
    /**
     * Operation failed error command bytes.
     */
    public static final byte[] ERROR_OPERATION_FAILED = { (byte) 0x63, 
                                                          (byte) 0x00 };
    
    /**
     * Function not supported error command bytes.
     */
    public static final byte[] ERROR_FUNCTION_NOT_SUPPORTED = { (byte) 0x6A, 
                                                                (byte) 0x81 };
    
    /**
     * Get UID command bytes.
     */
    public static final byte[] UID = { (byte) 0xFF, (byte) 0xCA, (byte) 0x00,
                                       (byte) 0x00, (byte) 0x00 };
    
    /**
     * Get ATS command bytes.
     */
    public static final byte[] ATS = { (byte) 0xFF, (byte) 0xCA, (byte) 0x01, 
                                       (byte) 0x00, (byte) 0x04 };
    
    /**
     * Load authentication key (slot 0) command bytes.
     */
    public static final byte[] LOAD_AUTHENTICATION_KEY_0 = { (byte) 0xFF, 
                                                              (byte) 0x82,
                                                              (byte) 0x00, 
                                                              (byte) 0x00, 
                                                              (byte) 0x06 };
    
    /**
     * Load authentication key (slot 1) command bytes.
     */
    public static final byte[] LOAD_AUTHENTICATION_KEY_1 = { (byte) 0xFF, 
                                                              (byte) 0x82,
                                                              (byte) 0x00,
                                                              (byte) 0x01, 
                                                              (byte) 0x06 };
    
    /**
     * 
     */
    public static final byte[] AUTHENTICATE_KEY = { (byte) 0xFF, 
                                                    (byte) 0x86,
                                                    (byte) 0x00,
                                                    (byte) 0x00,
                                                    (byte) 0x05,
                                                    (byte) 0x01,
                                                    (byte) 0x00 };
    
    /**
     * 
     */
    public static final byte AUTHENTICATE_KEY_TYPE_A = (byte) 0x60;
    
    /**
     * 
     */
    public static final byte AUTHENTICATE_KEY_TYPE_B = (byte) 0x61;
    
    /**
     * 
     */
    public static final byte AUTHENTICATE_KEY_LOCATION_0 = (byte) 0x00;
    
    /**
     * 
     */
    public static final byte AUTHENTICATE_KEY_LOCATION_1 = (byte) 0x01;
    
    
    
}
